import 'package:flutter/material.dart';

class AppColor {
  static const Color darkGrey = Color.fromRGBO(36, 41, 45, 1);
  static const Color lightGrey = Color.fromRGBO(142, 146, 149, 1);
  static const Color gold = Color.fromRGBO(241, 218, 111, 1);
}
